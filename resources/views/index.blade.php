<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100 ,600" rel="stylesheet" type="text/css">

    </head>
    <body>
    <div class="row">
        <div class="container">
           <table class="table table-stripped" id="product">
               <thead>
               <tr>
                   <th>Nombre</th>
                   <th>Marca</th>
                   <th>Color</th>
                   <th>Comentario</th>
                   <th>Fecha de creación</th>
                   <th>Acción</th>
               </tr>
               </thead>
               <tbody>
                    @foreach($product as $product)
                        <tr>
                            <th>{{$product->name}}</th>
                            <th>{{$product->mark}}</th>
                            <th>{{$product->color}}</th>
                            <th>{{$product->comment}}</th>
                            <th>{{$product->created_at->diffForHumans()}}</th>
                            <th>{{$product->updated_at->diffForHumans()}}</th>
                            <th style="display: flex;">
                                {!! Form::open(['route'=>['product.destroy',$product->id],'method'=>'DELETE','class'=>'form-delete']) !!}
                                    {!! Form::submit('Borrar',['class'=>'btn btn-danger','data-id'=>$product->id]) !!}
                                {!! Form::close() !!}
                                {{--normal--}}
                                {{--{!! link_to_action('ProductController@edit',$title="Edit",$attributes=array('id'=>$product->id),$parameters=array('class'=>'btn btn-primary','data-id'=>$product->id)) !!}--}}
                                {{--ajax--}}
                                {!! Form::button('Editar',['class'=>'btn btn-primary form-edit','data-toggle'=>'modal','data-target'=>'.modal-edit','data-id'=>$product->id]) !!}
                            </th>
                        </tr>
                        @endforeach
               </tbody>
           </table>
        </div>
    </div>
    {{--modal de edición--}}
    <div class="modal fade modal-edit" tabindex="-1" role="dialog" aria-labelledby="modal-edit">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    {!! Form::open(['method'=>'PUT','class'=>'form form-update']) !!}
                    <div class="form-group">
                        {!! Form::text('name',null,['placeholder'=>'Nombre de producto','class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::text('mark',null,['placeholder'=>'Nombre de la marca','class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::text('color',null,['placeholder'=>'Color','class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::textarea('comment',null,['placeholder'=>'Comentario','class'=>'form-control']) !!}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        {!! Form::submit('Actualizar',['class'=>'btn btn-primary btn-update','data-id'=>'']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    {{--fin de modal de edicion--}}
    </body>
    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
</html>