$('.form-create').submit(function( event ) {
    event.preventDefault();
    $.ajax({
        url: '/product/',
        type: 'post',
        data: $(this).serialize(), // Remember that you need to have your csrf token included
        dataType: 'json',
        success: function( data ){
            alert(data.return);
        },
        error: function( data ){
            var errorString='';
            $.each(data.responseJSON,function(key,value){
                errorString+='<li>'+value+'</li>'
            });
            $('#errors').html(errorString);
            $('#error-container').fadeIn();
            $('.close').click(function(){
               $('#error-container').fadeOut();
            });
        }
    });
});
$('.form-delete').submit(function(event){
    event.preventDefault();
    var btn = $(this).find('input[type=submit]');
    var id=btn.data("id");
    var row = btn.parents('tr');
    $.ajax({
        url: '/product/'+id,
        type: 'delete',
        data: $(this).serialize(), // Remember that you need to have your csrf token included
        dataType: 'json',
        success: function (data) {
            row.fadeOut();
            alert(data.return);
        },
        error: function (data) {
            // Handle error
        }
    });
});
$('.form-update').submit(function(event){
    event.preventDefault();
    var id = $(this).find('input[type=submit]').data("id");
    $.ajax({
        url: '/product/'+id,
        type: 'put',
        data: $(this).serialize(), // Remember that you need to have your csrf token included
        dataType: 'json',
        success: function (data) {
            alert('actualizado');
            location.reload();
        },
        error: function (data) {
            alert('registro no encontrado');
        }
    });
});
$('.form-edit').click(function(){
    var id = $(this).data("id");
    $.ajax({
        url: '/product/'+id+'/edit',
        type: 'get',
        dataType: 'json',
        success: function (data) {
            $(".modal-edit input[name='name']").val(data.name);
            $(".modal-edit input[name='mark']").val(data.mark);
            $(".modal-edit input[name='color']").val(data.color);
            $(".modal-edit textarea[name='comment']").val(data.comment);
            $(".modal-edit .form-update .btn-update").data('id',data.id);
        },
        error: function (data) {
            // Handle error
        }
    });
});