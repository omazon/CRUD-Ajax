<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100 ,600" rel="stylesheet" type="text/css">

    </head>
    <body>
    <div class="row">
        <div class="container">
            {!! Form::open(['route'=>['product.update',$product->id],'method'=>'PUT','class'=>'form form-update']) !!}
            <div class="form-group">
                {!! Form::text('name',$product->name,['placeholder'=>'Nombre de producto','class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::text('mark',$product->mark,['placeholder'=>'Nombre de la marca','class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::text('color',$product->color,['placeholder'=>'Color','class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::textarea('comment',$product->comment,['placeholder'=>'Comentario','class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::submit('Actualizar',['class'=>'btn btn-primary btn-update','data-id'=>$product->id]) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    </body>
    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
</html>
