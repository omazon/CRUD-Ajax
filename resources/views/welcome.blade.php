<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <!-- Fonts -->
        {{--<link href="https://fonts.googleapis.com/css?family=Raleway:100 ,600" rel="stylesheet" type="text/css">--}}

    </head>
    <body>
    <div class="row">
        <div class="container">
            <div id="error-container" class="alert alert-danger alert-dismissable" hidden>
                <a href="#" class="close" aria-label="close">&times;</a>
                <div id="errors"></div>
            </div>
        @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                    @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::open(['route'=>'product.store','method'=>'POST','class'=>'form form-create']) !!}
            <div class="form-group">
                {!! Form::text('name',null,['placeholder'=>'Nombre de producto','class'=>'form-control','required'=>'required']) !!}
            </div>
            <div class="form-group">
                {!! Form::text('mark',null,['placeholder'=>'Nombre de la marca','class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::text('color',null,['placeholder'=>'Color','class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::textarea('comment',null,['placeholder'=>'Comentario','class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::submit('Registrar',['class'=>'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    </body>
    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
</html>
